.model small
.code
org 100h
mulai:
    mov ah,02 ; servis cetak karakter
    mov dl,65 ; dl=A
    mov cx,26 ; banyaknya perulangan
ulang:
    int 21h ; cetak karakter
    loop ulang ; ulangi hingga 26 kali
    int 20h ; selesai
end mulai